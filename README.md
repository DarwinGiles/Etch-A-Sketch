# Etch-a-Sketch

An online etch-a-sketch built with Javascript.

Created as part of The Odin Project's [Curriculum.](https://www.theodinproject.com/courses/web-development-101/lessons/etch-a-sketch-project?ref=lnav)