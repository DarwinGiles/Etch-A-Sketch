var sketchBox = document.querySelector("#sketch-box");

function generateBlocks(size)
{
    document.querySelectorAll(".sketch-square").forEach(square => sketchBox.removeChild(square));

    var pxSize = 640 / size;
    for (y = 0; y < size; y++)
    {
        for (x = 0; x < size; x++)
        {
            var box = document.createElement("div");
            sketchBox.appendChild(box);
            box.classList.add("sketch-square")
            box.addEventListener("mouseenter", onBoxHovered);
            box.style.width = pxSize + "px";
            box.style.height = pxSize + "px";
        }
    }
}

function resetBlocks()
{
    document.querySelectorAll(".sketch-square").forEach(square => square.classList.remove("filled"));
}

function onBoxHovered(e)
{
    this.classList.add("filled");
}

function getNewDimensions()
{
    var newSize = prompt("Enter the new grid size.");
    generateBlocks(+newSize);
}

generateBlocks(16);
document.querySelector("#reset-button").addEventListener("click", resetBlocks);
document.querySelector("#resize-button").addEventListener("click", getNewDimensions);

